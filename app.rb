require 'sinatra'
require 'json'
require_relative 'models/key_value_pair'
require_relative 'models/database_connection'

current_env = ENV['RACK_ENV'] || 'development'
DatabaseConnection.connect current_env

before do
  content_type :json
  headers 'Access-Control-Allow-Origin' => '*',
          'Access-Control-Allow-Methods' => %w(OPTIONS GET POST)
end

get '/' do
  'site root'
end

get '/:key' do
  kvp = KeyValuePair.where(key: params[:key]).first
  kvp.nil? ? 404 : { kvp.key => kvp.val }.to_json
end

put '/:key/?' do
  kvp = KeyValuePair.where(key: params[:key]).first
  return 404 if kvp.nil?

  kvp.update_attributes(val: params['val'])
  200
end
