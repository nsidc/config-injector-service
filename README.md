# PROJECT STATUS

**The only known user of the config-injector service is the Swagger implementation. It should not be used for future projects. Consult the current development team tech radar (and the development team) for recommended approaches to credential handling.**

## Configuration Injector Service

This service is used to store and look up configuration values.  The motivation
behind this service is to have a secure way to store and retrieve potentially
sensitive information.  By calling a service to retrieve this information, we
can publicly expose our code while still maintaining confidentiality of
organizational secrets.

The service currently exposes one endpoint:

* GET /:key - Get the configuration value for a configuration key by name

## Developer info

### Reading logs
```bash
ssh gas
become scm
cd /disks/scm/jenkins/jobs/Config_Injector_Service_VM_1a_Provision_Integration/workspace/
vagrant ssh integration
cd /opt/config_injector_service/run/log
```

## To build and run

### Building

```bash
git clone git@bitbucket.org:nsidc/config-injector-service.git
cd ./config-injector-service/
bundle install
```

### Migrating the db

```bash
rake db:migrate RACK_ENV=${RACK_ENV} VERSION=${MIGRATION_VERSION}
```

### Command line tool

This project uses `tux` as a command line tool to interactively test
methods or interact with the database.

```bash
export RACK_ENV=development
tux
```

Will start the app and connect to the development database as configured in the
database.yml file, which is in the .gitignore for this project as it contains
database connection information.

### Run

```bash
ruby ./app.rb
```

## To test

```bash
rake spec:unit
```

## Design

The service is basically just an interface to a database.  It is currently using
a relational database, but it could also use a key/value store like Redis or
MongoDB.

This is a lightweight ruby service that uses Sinatra and a puma web server.

## Organization Info

### How to contact NSIDC

User Services and general information:  
Support: http://support.nsidc.org  
Email: nsidc@nsidc.org  

Phone: +1 303.492.6199  
Fax: +1 303.492.2468  

Mailing address:  
National Snow and Ice Data Center  
CIRES, 449 UCB  
University of Colorado  
Boulder, CO 80309-0449 USA  

### License

Every file in this repository is covered by the GNU GPL Version 3; a copy of the
license is included in the file COPYING.

### Citation Information

Chris Chalstrom and Michael Brandt (2014): Configuration Injector Service. The
National Snow and Ice Data Center. Software.
http://ezid.cdlib.org/id/doi:10.7265/N5DZ0684