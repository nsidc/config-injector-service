require 'yaml'
require 'sinatra/activerecord'

class DatabaseConnection
  class << self
    def connect(env)
      db_yml_path = File.join(File.dirname(__FILE__), '..', 'config', 'database.yml')

      # db_yml is written via jenkins job - if it is not found,
      # then this is the test job so load a sqlite3 test db.
      if File.exist? db_yml_path
        db_config = YAML.load(File.read(db_yml_path))
        ActiveRecord::Base.establish_connection db_config[env]
      else
        db_config = { adapter: 'sqlite3', database: 'testdb.sqlite3' }
        ActiveRecord::Base.establish_connection db_config
      end
    end
  end
end
