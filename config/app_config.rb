module AppConfig
  APPLICATION_NAME = 'config-injector-service'
  APPLICATION_DIR = '/opt/config_injector_service'

  def current_env
    ENV['RACK_ENV'] || 'development'
  end

  def self.[](key)
    key_sym = key.to_sym unless key.nil?
    key_sym = :development if APPLICATION_ENVIRONMENTS[key_sym].nil?

    APPLICATION_ENVIRONMENTS[key_sym]
  end

  private

  COMMON = {
    enricher_thread_count: 5,
    port: '10680',
    num_workers: 5,
    pidfile: '/opt/config_injector_service/run/puma.pid',
    state_path: '/opt/config_injector_service/run/puma.state',
    std_err_log: '/opt/config_injector_service/run/log/puma.stderr.log',
    std_out_log: '/opt/config_injector_service/run/log/puma.stdout.log'
  }

  APPLICATION_ENVIRONMENTS = {
    development: COMMON.clone,
    integration: COMMON.clone,
    qa: COMMON.clone,
    staging: COMMON.clone,
    production: COMMON.clone
  }
end
