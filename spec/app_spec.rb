# This sets RACK_ENV to test, so no real data is mutated.
require File.join(File.dirname(__FILE__), '.', 'spec_helper')

describe 'get /:key' do

  # Make sure we are working from a clean slate.
  before(:each) do
    KeyValuePair.destroy_all
  end

  # After tests have run, the test db should be empty.
  after(:each) do
    KeyValuePair.destroy_all
  end

  it 'should give 404 if key is not found' do
    get '/test.null.key'
    last_response.should_not be_ok
    last_response.status.should eq(404)
  end

  it 'should retrieve the key value pair' do
    record_key = 'my.test.key'
    record_val = 'my.test.val'
    KeyValuePair.create(key: record_key, val: record_val)

    expect(KeyValuePair.all.size).to eq(1)

    get "/#{ record_key }"
    last_response.should be_ok

    response = JSON.parse(last_response.body)
    response[record_key].should eq(record_val)
  end

end

describe 'put /:key' do

  test_attrs = {
    key: 'barfoo',
    val: 'foobar'
  }

  before(:each) do
    KeyValuePair.create test_attrs
  end

  after(:each) do
    KeyValuePair.destroy_all
  end

  it 'should return 200' do
    put "/#{ test_attrs[:key] }",
        { val: 'foobar' },
        accept: 'application/json'

    last_response.should be_ok
  end

  it 'should mutate the val properly' do
    new_key_value = 'arfoob'
    put "/#{ test_attrs[:key] }",
        { val: "#{ new_key_value }" },
        accept: 'application/json'

    last_response.should be_ok

    get "/#{ test_attrs[:key] }"
    last_response.should be_ok

    response = JSON.parse(last_response.body)
    response[test_attrs[:key]].should eq(new_key_value)
  end

end
