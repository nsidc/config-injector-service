# http://recipes.sinatrarb.com/p/testing/rspec

# Don't change this or real data could
# get deleted when tests are run.
ENV['RACK_ENV'] = 'test'

require File.join(File.dirname(__FILE__), '..', 'app')
require 'rack/test'

module RSpecMixin
  include Rack::Test::Methods
  def app
    Sinatra::Application
  end
end

RSpec.configure { |c| c.include RSpecMixin }
