class CreateKeyValuePairs < ActiveRecord::Migration
  def change
    create_table :key_value_pairs do |t|
      t.string :key
      t.string :val
    end
  end
end
